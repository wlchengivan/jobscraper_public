from numpy.core.fromnumeric import reshape
import requests
from bs4 import BeautifulSoup
import pandas as pd
import urllib, json, requests
from textblob import TextBlob
import sys
import tweepy
import matplotlib.pyplot as plt
import numpy as np
import ast
import os



def getJobId(jobTitle, count):
    listurl = "https://hk.jobsdb.com/hk/search-jobs/"
    
    req = requests.get(listurl + jobTitle + "/" + str(count + 1))
    soup = BeautifulSoup(req.content, 'html.parser')

    div_tags = soup.find_all("h1", class_="z1s6m00 _1hbhsw64u y44q7i0 y44q7i3 y44q7i21 y44q7ii", limit=30)

    temp=str(div_tags).split('href="')

    for x in range(1, len(temp)):
        temp2=temp[x].split('?token')
        #print(temp2[0])
        jobid[count * 30 + x - 1]=temp2[0]

    #jobid = np.delete(jobid, 0)
    

def getJobDetail():
    detailUrl="https://hk.jobsdb.com"
    global jobDetail

    jobDetail = np.empty((len(jobid), 4), dtype=object)
    
    print(jobid)
    
    for x in range(0, len(jobid)):
    #for x in range(1, 10):
        #print(x)
        #print(jobid[x])
        if(jobid[x] != None):
            jobDetail[x][0] = ""
            jobDetail[x][1] = ""
            jobDetail[x][2] = ""
            jobDetail[x][3] = ""

            req = requests.get(detailUrl + jobid[x])
            soup = BeautifulSoup(req.content, 'html.parser')

            jobTitle = soup.find_all("h1", class_="z1s6m00 _1hbhsw64u y44q7i0 y44q7il _1d0g9qk4 y44q7is y44q7i21")[0].string
            companyName = soup.find_all("span", class_="z1s6m00 _1hbhsw64u y44q7i0 y44q7i2 y44q7i21 _1d0g9qk4 y44q7ia")[0].string
            try:
                location = soup.find_all("span", class_="z1s6m00 _1hbhsw64u y44q7i0 y44q7i1 y44q7i21 y44q7ii")[0].string
            except:
                location = None

            

            jobDetail[x][0] = jobTitle
            jobDetail[x][1] = companyName
            jobDetail[x][2] = location

            try:
                jobHighlight = soup.find("ul").find_all("span", class_="z1s6m00 _1hbhsw64u y44q7i0 y44q7i1 y44q7i21 _1d0g9qk4 y44q7ia")
                for y in range(0, len(jobHighlight)):
                    print(jobHighlight[y].string)
                    try:
                        jobDetail[x][3] = jobDetail[x][3]+ str(y+1) + ": " + jobHighlight[y].string + "   "
                    except:
                        jobDetail[x][3] = ""
            except:
                jobHighlight = None

        
            
        
    for x in range(len(jobDetail) -1, 1, -1):
            if(jobDetail[x][0] == None):
                np.delete(jobDetail, x, 0)
        
    

def insertToDataFrame(query):    
    #np.delete(jobDetail, 0, 0)
    data = pd.DataFrame(jobDetail, columns=['Job Title', 'Company', 'Location', 'Job Highlights'])
    data = data.dropna(how = 'all')
    print(data)
    data = data.iloc[0:]
    data.to_csv(query + ".csv")

def getJobTotal(jobTitle):
    listurl = "https://hk.jobsdb.com/hk/search-jobs/"
    
    req = requests.get(listurl + jobTitle + "/1")
    soup = BeautifulSoup(req.content, 'html.parser')
    
    jobTotal = soup.find("span", class_="z1s6m00 _1hbhsw64u y44q7i0 y44q7i1 y44q7i21 _1d0g9qk4 y44q7i7")
    jobTotalTemp = str(jobTotal).split("</strong> of ")
    jobTotalTemp2 = jobTotalTemp[1].split(" jobs </span>")

    print("jobtotal is " + str(jobTotalTemp2[0].replace(',', '')))

    global jobid
    jobid = np.empty(int(jobTotalTemp2[0].replace(',', '')) + 1, dtype=object)


keyword="tvb"
getJobTotal(keyword)
#getJobId("data-engineer", 0)
count = 0
for x in range(0 , int(len(jobid)/30) + 1):
#for x in range(0 , 1):
    print("page" + str(x+1))
    getJobId(keyword, x)
    
print(jobid)
getJobDetail()
print("getJobDetail")

insertToDataFrame(keyword)
print("insertToDataFrame")